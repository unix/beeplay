#!/bin/bash

INTERPRET="$1"
SKLADBA="$2"
TIME=10000 # v milisekundach

who | grep -w ':[0-9]' | awk '{print $1, $2}' | while read USER DISPLAY; do
    ID=$(id -u $USER)
    sudo -u $USER DISPLAY=$DISPLAY DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$ID/bus notify-send -i audio-volume-high -u low -t $TIME 'Beeplay' "$INTERPRET:\n$SKLADBA" &
done
