# PC Speaker Beep Suite

This repository contains tools to convert MIDI files to text files that
can be played on various computers simultaneously, creating an illusion
of polyphony.

## Overview

The repository contains the following scripts:

* `beeplay` is the core script that plays the selected track
* `polybeep` takes care of selecting the right track for a given
  host (based on track ID and a number extracted from the hostname)
* `midiconv` converts MIDI files to `beeplay` tracks

There is also a `Makefile` that automates the MIDI files conversion.

## Dependencies

* PERL v5.24
* PERL modules (not in core) to play the music:
  * [`DateTime`](https://metacpan.org/pod/DateTime)
  * [`POSIX::strptime`](https://metacpan.org/pod/POSIX::strptime)
  * [`IPC::System::Simple`](https://metacpan.org/pod/IPC::System::Simple)
* PERL modules (not in core) to convert MIDI files:
  * [`MIDI`](https://metacpan.org/pod/MIDI)

## How to...

### ...add a new score

1. **Legally** obtain a MIDI file to convert.
2. Save it in the `midi/` directory with a reasonable, but short name.
   The file extension must be `mid`.
3. Run `make` to convert all MIDI files or just `make gen-BASENAME`
   to only generate tracks from `midi/BASENAME.mid`
4. Done.

### ...play score on multiple computers

1. Make sure the target system has the `pcspkr` module available
   (it needs not to be loaded though).
2. Make sure the `beeplay` and `polybeep` scripts will work with your
   version of perl, try e.g. `perl -c SCRIPT`.
3. Clone or update the repository on each of the target computers.
4. Run the `polybeep TIME SCORE` command, where `TIME` is the
   time you want it to start playing (format is `HH:MM:SS` or use
   the string `now`) and `SCORE` is the name that will be searched
   for in the `tracks/` directory.
5. Profit!!!
