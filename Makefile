TRACKDIR = tracks
MIDIDIR  = midi

MIDICONV = ./midiconv --precise
MKDIR    = mkdir -p

BASES    = $(shell cd $(MIDIDIR); find . -type f -name '*.mid' | sed -r 's!(^\./|\.mid$$)!!g')
TARGETS  = $(foreach BASE,$(BASES),$(TRACKDIR)/$(BASE)/metadata)

all: $(TARGETS)

test:
	echo $(MIDIFILES)
	echo $(STAMPS)

$(TRACKDIR)/%/metadata: $(MIDIDIR)/%.mid
	$(MKDIR) $(TRACKDIR)/$*
	$(RM) $(TRACKDIR)/$*/*.beep
	$(MIDICONV) --output="$(TRACKDIR)/$*/%id.beep" "$(MIDIDIR)/$*.mid" --meta="$@"


.PHONY: all clean

clean:
	$(RM) -r tracks/*/*.beep
