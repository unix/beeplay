# FI Concert Schedule

## A219

- 10:00 thunderstruck
- 14:00 super-mario
- 16:00 vivaldi-summer

## B130

- 10:00 dvorak-humoresque-7
- 12:00 joplin-the-entertainer
- 14:00 queen
- 16:00 smooth-criminal

## Hala

- 10:00 mambo-no5
- 11:00 rondo-ala-turka
- 12:00 scarlatti-sonata-1
- 13:00 we-will-rock-you
- 14:00 valkyrie
- 15:00 imperial-march
- 16:00 bach-toccata-fugue-bwv565
- 17:00 bburg1-1
- 18:00 ymca
- 19:00 rick-astley
